defmodule TextConsumer do
  @moduledoc """
  Documentation for `TextConsumer`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TextConsumer.hello()
      :world

  """
  def hello do
    :world
  end
end
