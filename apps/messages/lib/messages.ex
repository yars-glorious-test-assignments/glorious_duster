defmodule Messages do
  @moduledoc """
    This module defines Protobuf messages for microservices to communicate with.
  """

  use Protox, schema: """
    syntax = "proto3";

    package Messages;

    message TextMessage {
      string message_id = 1;
      string data = 2;
    }

    message ParsedTextMessage {
      string message_id = 1;
      uint64 size = 2;
    }

    message FailedTextParsingMessage {
      string message_id = 1;
      string reason = 2;
    }
  """
end
