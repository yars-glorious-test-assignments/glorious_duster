defmodule Producer.Server do
  @moduledoc """
    A server that sends text messages to the message bus and handles responses from consumers
  """
  use GenServer
  use AMQP

  import UUID

  @queue_name "text_producer_feed"

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl GenServer
  def init(_initial_state) do
    connection_string = Application.fetch_env!(:producer, :message_bus_url)
    {:ok, conn} = Connection.open(connection_string)
    {:ok, chan} = Channel.open(conn)
    setup_queue(chan)
    {:ok, %{broker: %{channel: chan, conn: conn}}}
  end

  defp setup_queue(channel) do
    {:ok, _} = Queue.declare(channel, @queue_name, durable: true)
  end

  @impl GenServer
  def handle_call({:publish, text}, _from, %{broker: %{channel: channel}} = state) when is_binary(text) do
    message_id = uuid4()
    encoded_text = encode_message!(message_id, text)
    :ok = AMQP.Basic.publish(channel, "", @queue_name, encoded_text,
      mandatory: true, persistent: true, message_id: message_id, type: "text_message"
    )
    {:reply, {:ok, message_id}, state}
  end

  @impl GenServer
  def handle_call({:publish, _text}, _from, state) do
    {:reply, {:error, :wrong_format}, state}
  end

  defp encode_message!(id, data) do
    {:ok, msg} = Protox.Encode.encode(%Messages.TextMessage{message_id: id, data: data})
    IO.iodata_to_binary(msg)
  end
end
