defmodule Producer.MixProject do
  use Mix.Project

  def project do
    [
      app: :producer,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixirc_paths: elixirc_paths(Mix.env()),
      elixirc_options: [warnings_as_errors: true],
      test_paths: test_paths(Mix.env()),
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  defp elixirc_paths(:test), do: [
    "lib",
    "test/support",
    "test/unit/support"
  ]
  defp elixirc_paths(:integration), do: [
    "lib",
    "test/support",
    "test/integration/support",
  ]

  defp elixirc_paths(_), do: ["lib"]

  defp test_paths(:integration), do: ["test/integration"]
  defp test_paths(_),            do: ["test/unit"]


  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Producer.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:protox, "~> 1.5"},
      {:messages, in_umbrella: true},
      {:stream_data, "~> 0.5"},
      {:amqp, "~> 3.0"},
      {:elixir_uuid, "~> 1.2"},
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      # {:sibling_app_in_umbrella, in_umbrella: true}
    ]
  end
end
