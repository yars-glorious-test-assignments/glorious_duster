defmodule Generators do
  @moduledoc """
    A module contains generators' defenitions used for property-based testing
  """
  @spec any_term ::
          StreamData.t(
            atom
            | binary
            | reference
            | [atom | binary | reference | number]
            | number
            | tuple
            | %{
                optional(atom | binary | reference | number) => atom | binary | reference | number
              }
          )
  def any_term() do
    StreamData.term()
  end

  @spec text_gen :: StreamData.t(binary)
  def text_gen() do
    StreamData.binary()
  end

  @spec any_but_binary_gen ::
  StreamData.t(
    atom
    | reference
    | [atom | binary | reference | number]
    | number
    | tuple
    | %{
        optional(atom | binary | reference | number) => atom | binary | reference | number
      }
  )
  def any_but_binary_gen() do
    any_term()
    |> StreamData.filter(fn term -> not is_binary(term) end)
  end
end
