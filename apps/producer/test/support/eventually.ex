defmodule Eventually do
  @moduledoc """
    This module contains public functions `eventually/1`, `eventually/2`, `eventually/3`,
    `eventually/4` which help assert match statements that might not match at the moment
    of first invocation, but is expected to match eventually in the future. It helps to
    wait for certain event in future without setting any fixed delay, as it gradually
    increases the delay between the match invocations.
  """
  @spec eventually((() -> any), integer(), integer(), integer()) :: any
  def eventually(callback, delay \\ 10, delay_step \\ 10, max_attempts \\ 20) do
    do_eventually(callback, delay, delay_step, max_attempts)
  end

  defp do_eventually(callback, _delay, _delay_step, attempts_left) when attempts_left <= 1 do
    callback.()
  end

  defp do_eventually(callback, delay, delay_step, attempts_left) do
    try do
      callback.()
    rescue
      _e in ExUnit.AssertionError ->
        :timer.sleep(delay)
        do_eventually(callback, delay + delay_step, delay_step, attempts_left - 1)
    end
  end
end
