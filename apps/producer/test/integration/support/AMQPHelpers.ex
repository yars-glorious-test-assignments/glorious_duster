defmodule AMQPHelpers do
  @moduledoc """
    A module contains helper for AMQP to be used in tests
  """
  def connect_to_broker(conn_descriptor) do
    {:ok, conn} = AMQP.Connection.open(conn_descriptor)
    {:ok, channel} = AMQP.Channel.open(conn)

    %{conn: conn, channel: channel}
  end

  defdelegate get(chan, queue), to: AMQP.Basic

  def clean_up_broker(chan, affected_queues) do
    for queue <- affected_queues do
      {:ok, _map} = AMQP.Queue.delete(chan, queue)
    end
  end
end
