defmodule ProducerServerTest do
  use ExUnitProperties
  use ExUnit.Case, async: false

  import AMQPHelpers
  import Eventually
  import Generators

  @subject Producer.Server
  @text_message_feed "text_producer_feed"
  @queues_affected_by_test [@text_message_feed]

  def connection_string() do
    Application.get_env(:producer, :message_bus_url)
  end

  def assert_queue_declared(chan, queue) do
    assert :ok = AMQP.Basic.publish(chan, "", queue, "check #{queue} exists")
    expected_reply = "check #{queue} exists"

    {:ok, ^expected_reply, _meta} = get(chan, queue)
  end

  setup_all do
    connect_to_broker(connection_string())
  end

  setup_all %{channel: channel} do
    clean_up_broker(channel, @queues_affected_by_test)
  end

  setup _context do
    %{subj_pid: start_supervised!(@subject)}
  end

  # creates state matching subject GenServer's state
  setup %{conn: conn, channel: channel} do
    %{state: %{broker: %{conn: conn, channel: channel}}}
  end

  setup %{channel: channel} do
    on_exit(fn -> clean_up_broker(channel, @queues_affected_by_test) end)

    :ok
  end

  describe "init/1" do
    test "initialized state by {:ok, %{connection: %{channel: %AMQP.Channel{}, conn: %AMQP.Connection{}}}} ignoring arguments" do
      assert {:ok, %{broker: broker}} = @subject.init(Enum.take(any_term(), 10))
      assert %{channel: %AMQP.Channel{}, conn: %AMQP.Connection{}} = broker
    end

    test "declares queues it publishes and reads from", %{channel: chan} do
      assert_queue_declared(chan, @text_message_feed)
    end
  end

  describe "handles call of {:publish, _pid, payload}" do
    property "returns {:reply, {:ok, id}, untouched_state} when binary is provided", %{state: state} do
      check all text <- text_gen() do
        assert {:reply, {:ok, _id}, ^state} = @subject.handle_call({:publish, text}, nil, state)
      end
    end

    property "returns unique id on every cast", %{state: state} do
      message_ids =
        text_gen()
        |> Enum.take(100)
        |> Enum.map(fn text ->
          {:reply, {:ok, id}, ^state} = @subject.handle_call({:publish, text}, nil, state)
          id
        end)

      uniq_message_ids =
        message_ids
        |> MapSet.new()
        |> MapSet.to_list()

      assert Enum.count(message_ids) == Enum.count(uniq_message_ids)
    end

    property "returns {:reply, {:error, :wrong_format}, untouched_state} for any type of input but binary", %{
      state: state
    } do
      check all not_text <- any_but_binary_gen() do
        assert {:reply, {:error, :wrong_format}, ^state} = @subject.handle_call({:publish, not_text}, nil, state)
      end
    end

    test "publishes encoded message to the queue with metadata", %{channel: channel, subj_pid: pid}  do
      text = ExUnitProperties.pick(text_gen())
      {:ok, id} = GenServer.call(pid, {:publish, text})
      encoded_message = Protox.Encode.encode!(%Messages.TextMessage{message_id: id, data: text})
      expected_message = IO.iodata_to_binary(encoded_message)

      eventually(fn ->
        assert {:ok, ^expected_message, %{message_id: ^id, type: "text_message"}} = get(channel, @text_message_feed)
      end)
    end
  end
end
