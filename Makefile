ut:
	mix test --trace --max-failures 1 --slowest 5

it:
	MIX_ENV=integration mix test --trace --max-failures 1 --slowest 5