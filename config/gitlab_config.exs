import Config

config :producer,
  message_bus_url: "amqp://user:password@bitnami-rabbitmq"
